
enclave {

	include "stdbool.h"

	include "sgx_tcrypto.h"
	include "sgx_quote.h"
	include "teex_mutual_ra.h"

	from "sgx_tsgxssl.edl" import *;

	untrusted {
		teex_mutual_ra_status_t teex_do_mutual_attestation(sgx_enclave_id_t eid, int socketfd, teex_mutual_ra_context_t ra_ctx, [in, string] char *ias_client_cert_file, [in, string] const char *ias_client_key_file, bool involve_ias) allow (teex_mutual_ra_get_nonce, teex_mutual_ra_create_eckey, teex_mutual_ra_proc_remote_msg1_trusted, teex_mutual_ra_get_msg2_trusted, teex_mutual_ra_proc_remote_msg2_trusted, teex_mutual_ra_get_msg3_trusted, teex_mutual_ra_proc_remote_msg3_trusted);
	};
	trusted {
		public teex_mutual_ra_status_t teex_mutual_ra_get_nonce(teex_mutual_ra_context_t teex_mutual_ra_ctx, [out] sgx_quote_nonce_t *p_nonce);
		public teex_mutual_ra_status_t teex_mutual_ra_create_eckey(teex_mutual_ra_context_t teex_mutual_ra_ctx, [out] sgx_ec256_public_t *p_public);
		public teex_mutual_ra_status_t teex_mutual_ra_proc_remote_msg1_trusted(teex_mutual_ra_context_t teex_mutual_ra_ctx, [in] const teex_mutual_ra_msg1_t *p_remote_msg1);
		//public void teex_mutual_ra_create_report(teex_mutual_ra_context_t teex_mutual_ra_ctx, [in] const sgx_target_info_t *p_qe_target, [out] sgx_report_t *p_report);
		public teex_mutual_ra_status_t teex_mutual_ra_get_msg2_trusted(teex_mutual_ra_context_t teex_mutual_ra_ctx, [in, size=sig_rl_size] const uint8_t *sig_rl, uint32_t sig_rl_size, [out, size=msg2_len] teex_mutual_ra_msg2_t *p_msg2, uint32_t msg2_len);
		public teex_mutual_ra_status_t teex_mutual_ra_proc_remote_msg2_trusted(teex_mutual_ra_context_t teex_mutual_ra_ctx, [in, size=remote_msg2_len] const teex_mutual_ra_msg2_t *p_remote_msg2, uint32_t remote_msg2_len, [in] const sgx_target_info_t *p_qe_target, [out] sgx_report_t *p_report);
		public teex_mutual_ra_status_t teex_mutual_ra_get_msg3_trusted(teex_mutual_ra_context_t teex_mutual_ra_ctx, [in, size=quote_len] const sgx_quote_t *p_quote, uint32_t quote_len, [in] const sgx_report_t *p_qe_report, [out, size=msg3_len] teex_mutual_ra_msg3_t *p_msg3, uint32_t msg3_len);
		//public void teex_mutual_ra_verify_remote_report(teex_mutual_ra_context_t teex_mutual_ra_ctx, [in, size=quote_size] const sgx_quote_t *p_remote_quote, uint32_t quote_size, [in, string] const char *report_body_from_ias, [in, string] const char *signature, [in, string] const char *certchain_urlencoded);
		public teex_mutual_ra_status_t teex_mutual_ra_proc_remote_msg3_trusted(teex_mutual_ra_context_t teex_mutual_ra_ctx, [in, size=remote_msg3_len] const teex_mutual_ra_msg3_t *p_remote_msg3, uint32_t remote_msg3_len, [in, string] const char *report_body_from_ias, [in, string] const char *signature_base64ed, [in, string] const char *certchain_urlencoded);
	};

};

