
#ifndef ETHPRC_H
#define ETHPRC_H

#include <iostream> 

#include "ethrpc_type.h"
#include "rpc.h"

using namespace std;


class ETHRPC{

public:

    ETHRPC(const char* url);
    // ~ETHRPC();
    ResultCode  web3_sha3(const char* raw_ascii,  string *res);
    ResultCode  eth_call(const char* contract_addr, const char* func_spec, int paras_count, int return_count, ...);
    ResultCode  eth_send(const char* contract_addr, const char* from_pk, int gas, int gas_price, int value, const char* func_spec, string* txnhash,  int paras_count, ...);
    ResultCode  eth_getTransactionReceipt(const char* txnhash, string *res);
    ResultCode  personal_importRawKey(const char* private_key,  const char* passphrase, string *res);
    ResultCode  personal_lockAccount(const char* address, const char* passphrase, int duration = 300);
    
private:

    RPC* rpc;

};



#endif
