SGX_SDK ?= /opt/intel/sgxsdk
#OPENSSL_HOME ?= /opt/openssl/1.1.0i/
TEEX_SSL_HOME ?= ./deps/TEEX-SSL
PUBLIC_CHAIN_SDK_HOME ?= ./deps/PublicChainSDK
CC  := gcc
CXX := g++
AR := ar


C_FLAGS := -g -Wall -I./include -I$(SGX_SDK)/include -I$(TEEX_SSL_HOME)/include -I$(PUBLIC_CHAIN_SDK_HOME)/include 
#C_FLAGS += -fsanitize=address 
ifneq ($(OPENSSL_HOME), )
	C_FLAGS += -I$(OPENSSL_HOME)/include
endif
CPP_FLAGS := $(C_FLAGS) -std=c++11

LINK_FLAGS := -L./lib -L$(TEEX_SSL_HOME)/lib -L$(PUBLIC_CHAIN_SDK_HOME)/lib -lteex_ssl -lcurl -lpublicchain -lethrpc -lcurl -lcjson -luuid 
#LINK_FLAGS += -fsanitize=address
ifneq ($(OPENSSL_HOME), )
	LINK_FLAGS += -L$(OPENSSL_HOME)/lib -lcrypto
else
	LINK_FLAGS += -lcrypto
endif
#LINK_FLAGS := -L../Lib -lteex_ssl -l:libcrypto.a -lcurl -lpthread -ldl

LIB_NAME := ./lib/libteex.a


TEST_C_FILES := $(wildcard tests/*.c)
TEST_C_OBJECTS := $(TEST_C_FILES:.c=.o)
TEST_CPP_FILES := $(wildcard tests/*.cpp)
TEST_CPP_OBJECTS := $(TEST_CPP_FILES:.cpp=.o)

TEST_LINK_FLAGS := -L./lib -lteex $(LINK_FLAGS)




LIB_C_FILES := $(wildcard src/*.c)
LIB_C_OBJECTS := $(LIB_C_FILES:.c=.o)
LIB_CPP_FILES := $(wildcard src/*.cpp)
LIB_CPP_OBJECTS := $(LIB_CPP_FILES:.cpp=.o)


all: $(LIB_NAME) test

libs: $(LIB_NAME)


#%.o: %.c
#	$(CC) $(C_FLAGS) -c $< -o $@

%.o: %.cpp
	$(CXX) $(CPP_FLAGS) -c $< -o $@

$(LIB_NAME): $(LIB_C_OBJECTS) $(LIB_CPP_OBJECTS)
	$(AR) -rc $@ $^

test: $(TEST_C_OBJECTS) $(TEST_CPP_OBJECTS)
	$(CXX) $^ -o $@ $(TEST_LINK_FLAGS)


clean:
	rm -f $(LIB_NAME) test $(LIB_C_OBJECTS) $(LIB_CPP_OBJECTS) $(TEST_C_OBJECTS) $(TEST_CPP_OBJECTS)

