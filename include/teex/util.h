#ifndef __TEEX_UTIL_H__
#define __TEEX_UTIL_H__

#include "teex_ssl.h"

int isNumberStr(char *str);

char* teexHexStrDup(char *str, int size);

int teexGetLine(TEEX_SSL *ts, char *buf, int size);

char *sha256AndToHexStr(char *msg, size_t mlen) ;

int teexConnect(char *addr, int port);

#endif
