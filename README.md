# HCloud

### 介绍
HCloud是一个分布式可信云际计算平台。
HCloud致力于跨云环境下的可信数据融合。目前各云平台上储存了来自不同个人、企业用户的大量数据。由于安全、合规等不同问题，这些数据难以进行融合使用，极大的限制了数据所能够产生的价值。本项目利用密码学、TEE等技术，提供了一个跨云的可信计算环境，来自多个云平台的数据能够在可信环境中进行处理，从而获得数据融合之后的结果。

### 软件架构
HCloud平台允许开发者上传 **Service** 并且允许普通用户通过发起一个 **Task** 进行 **Service** 的调用。整个平台的所有信息都会记录在一个区块链上。

平台主要分为四个角色，ServiceManager，Dispatcher，Worker 以及 User。

 **ServiceManager** 
负责Service的管理，允许开发者上传服务代码，同时允许Worker下载服务代码。

**Dispatcher**
负责Worker的调度，接受用户的Task创建请求，挑选出合适的Worker并将其地址返回值用户。

**Worker**
执行用户发起的Task并返回执行结果。


### 安装教程

#### 0. 安装Intel SGX驱动
（https://github.com/intel/linux-sgx-driver）

#### 1. 获取HCloud容器

1.0 安装Docker

1.1 下载HCloud容器景象

```
    docker pull teex/teex-node:latest
```

1.2 启动容器

```
    docker run -it --device /dev/isgx teex/teex-node:latest
```

#### 2. 安装用户SDK

```
    git clone https://gitee.com/huazhichao/HCloud.git
```

#### 使用说明


#### 0. 启动HCloud节点

```
    cd HCLoud
    ./start_HCloud.sh
```

#### 1. 编译用户SDK

```
    cd HCloud
    make libs
```

#### 2. 编译测试Demo

```
    make test
```

#### 3. 运行测试Demo

```
    ./test
```
